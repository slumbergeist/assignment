import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { DataService } from './data.service';
import { FooterComponent } from './footer/footer.component';


@NgModule({
	declarations: [
		AppComponent,
		HeaderComponent,
		FooterComponent
	],
	imports: [
		BrowserModule,
	],
	providers: [
		DataService,
		Title
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
