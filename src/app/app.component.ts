import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';
import { Title } from '@angular/platform-browser';
import {
	trigger,
	state,
	style,
	animate,
	transition
} from '@angular/animations';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	
})
export class AppComponent implements OnInit {
	fund: any;
	title = '';
	activeStory: boolean = true;
	constructor(private titleService: Title, private data: DataService) {}

	ngOnInit() {
		this.fund = this.data.getData();
		this.titleService.setTitle(this.fund.title);
	}

	
}
